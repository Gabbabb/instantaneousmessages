using System.Threading.Tasks;
using System.Text;
using System.Net.WebSockets;
using System.Threading;

namespace InstantaneousMessages
{
    public class WebSocketDaemon 
    {
        public WebSocket Socket { get; set; }
        public async Task Send(string message)
        {
            if (Socket is null || Socket.State == WebSocketState.Closed)
                return;
            
            var bufferForSocket = new byte[1024*4];
            bufferForSocket = Encoding.UTF8.GetBytes(message);            
            await Socket.SendAsync(bufferForSocket,
            WebSocketMessageType.Text,
            true,
            CancellationToken.None);
        }

        internal bool WebSocketStateOpen()
        {
            return Socket.State == WebSocketState.Open;
        }
    }
}
