using System;
using System.Collections.Generic;
using InstantaneousMessages.Controllers;
using Microsoft.Extensions.Logging;

namespace InstantaneousMessages.Database
{
    public class DatabaseDaemon
    {
        IDatabaseDaemonJr database;
        readonly ILogger<DatabaseDaemon> _logger;

        public DatabaseDaemon()
        {
            
        }

        public DatabaseDaemon(ILogger<DatabaseDaemon> logger, PostgreDaemon database)
        {
            this._logger = logger;
            this.database = database;
        }

        public void CreateMessage(Message message)
        {            
            _logger.LogInformation("DatabaeDaemon trying to create record about new message");
            database.Add(message);
        }

        public List<Message> GetMessagesByTimeRange(TimeRange timeRange)
        {
            return database.GetMessagesByTimeRange(timeRange);       
        }
    }
}