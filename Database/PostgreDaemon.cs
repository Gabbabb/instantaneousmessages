using System.Collections.Generic;
using Microsoft.Extensions.Logging;
using Npgsql;

namespace InstantaneousMessages.Database
{
    public class PostgreDaemon : IDatabaseDaemonJr
    {
        readonly NpgsqlConnection _connection;
        private readonly ILogger<PostgreDaemon> _logger;
        public PostgreDaemon(ILogger<PostgreDaemon> logger)
        {
            this._connection = new NpgsqlConnection("Server=127.0.0.1;Port=5432;User Id=postgres;Password=415263;database=instantmessages");
            this._logger = logger;
        }

        public void Add(Message message)
        {
            _logger.LogInformation("Postgre trying to connect using: " + _connection.ConnectionString);
            _connection.Open();
            using (var cmd = new NpgsqlCommand("INSERT INTO public.message (no, time, content) VALUES (@n,@t,@c)", _connection))
            {
                cmd.Parameters.AddWithValue("n", message.No);
                cmd.Parameters.AddWithValue("t", message.Time);
                cmd.Parameters.AddWithValue("c", message.Content);
                _logger.LogInformation("Postgre trying to execute: " + cmd.CommandText);
                cmd.ExecuteNonQuery();
            }
            _connection.Close();
        }

        public List<Message> GetMessagesByTimeRange(TimeRange timeRange)
        {
            List<Message> result = new List<Message>();
            _connection.Open();
            using (var cmd = new NpgsqlCommand("SELECT * FROM message WHERE time >= @from AND time <= @to", _connection))
            {
                //string postgreTimstampFormat = "yy-MM-dd H:mm:ss";             
                cmd.Parameters.AddWithValue("from", timeRange.From);
                cmd.Parameters.AddWithValue("to", timeRange.To);
                _logger.LogInformation("Postgre trying to execute: " + cmd.CommandText);
                _logger.LogInformation("from = {0}, to = {1}", timeRange.From, timeRange.To);
                using (var reader = cmd.ExecuteReader())
                {    
                    while (reader.Read())
                    {
                        _logger.LogInformation("selected message with content: {0}, no: {1}, time: {2}", reader[0], reader[1], reader[2]);
                        result.Add(new Message(reader.GetInt32(1), reader.GetString(0), reader.GetDateTime(2)));
                    }             
                        
                }
            }
            _connection.Close();
            return result;            
        }

        public List<Message> GetMessagesForLastTenMins()
        {
            var prevTenMins = new TimeRange();
            prevTenMins.To = System.DateTime.Now;
            prevTenMins.From = prevTenMins.To.AddMinutes(10);
            return GetMessagesByTimeRange(prevTenMins);
        }

        public void InitDb()
        {
            // _connection.Open();
            // using (var cmd = new NpgsqlCommand("CREATE TABLE IF NOT EXISTS message (no integer, time timestamp, content text);", _connection))
            //     cmd.ExecuteNonQuery();
        }
    }
}