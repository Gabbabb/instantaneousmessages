namespace InstantaneousMessages.Database
{
    public class Message
    {
        public Message(){}
        public Message(string content)
        {
            this.Content = content;
        }

        public Message(int no, string content, System.DateTime time)
        {
            this.No = no;
            this.Content = content;
            this.Time = time;
        }

        public int No { get; set; }
        public string Content { get; set; }
        public System.DateTime Time { get; set; }
    }
}