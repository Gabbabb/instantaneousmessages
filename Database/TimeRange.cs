using System;

namespace InstantaneousMessages.Database
{
    public class TimeRange
    {
        public TimeRange()
        {
        }

        public DateTime From { get; set; }
        public DateTime To { get; set; }
    }
}