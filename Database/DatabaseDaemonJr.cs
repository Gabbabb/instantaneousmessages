using System.Collections.Generic;

namespace InstantaneousMessages.Database
{
    public interface  IDatabaseDaemonJr
    {
        void InitDb();
        void Add(Message message);
        List<Message> GetMessagesByTimeRange(TimeRange timeRange);
        List<Message> GetMessagesForLastTenMins();
    }
}