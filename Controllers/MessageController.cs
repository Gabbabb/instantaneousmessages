using InstantaneousMessages.Database;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Text.Json;
using System.Net.WebSockets;
using Microsoft.AspNetCore.Http;
using System.Threading.Tasks;
using Microsoft.Extensions.Logging;

namespace InstantaneousMessages.Controllers
{
    [ApiController]
    [Route("api/[controller]")]
    public class MessageController : ControllerBase
    {
        private readonly DatabaseDaemon _databaseDaemon;
        private readonly WebSocketDaemon _webSocketDaemon;
        private readonly ILogger _logger;

        public MessageController(DatabaseDaemon databaseDaemon, WebSocketDaemon webSocketDaemon, ILogger<MessageController> logger)
        {
            this._databaseDaemon = databaseDaemon;
            this._webSocketDaemon = webSocketDaemon;
            this._logger = logger;
        }

        [HttpPost]
        [Consumes("application/json")]
        public async Task SendMessage([FromBody] Message message)
        {
            var timeNow = System.DateTime.Now;
            message.Time = timeNow;
            _logger.LogInformation("MessageController received Message #{0} at {1}", message.No, message.Time);
            _databaseDaemon.CreateMessage(message);
            await _webSocketDaemon.Send(JsonSerializer.Serialize(message));       
        }

        [HttpGet("byTimeRange")]
        // переделать на передачу даты в uri
        public List<Message> GetMessagesByTimeRange(DateTime from, DateTime to)
        {
            TimeRange timeRange = new TimeRange();
            timeRange.From = from;
            timeRange.To = to;
            _logger.LogInformation("MessageController GET by time from:{0} to:{1}",
            timeRange.From.ToString(), timeRange.To.ToString());   
            return _databaseDaemon.GetMessagesByTimeRange(timeRange);
        }
    }
}