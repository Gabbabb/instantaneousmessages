using System;
using System.Collections.Generic;
using System.Linq;
using InstantaneousMessages.Database;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.HttpsPolicy;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Logging;
using Microsoft.AspNetCore.SignalR;

namespace InstantaneousMessages
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            services.AddControllers();
            services.AddSingleton<DatabaseDaemon>();
            services.AddSingleton<WebSocketDaemon>();
            services.AddSingleton<PostgreDaemon>();
            services.AddCors(options => options.AddPolicy("CorsPolicy",
            builder => 
            {
                builder.AllowAnyOrigin();
            }));
            services.AddLogging(loggingBuilder =>
            {
                loggingBuilder.AddConfiguration(Configuration.GetSection("Logging"));
                loggingBuilder.AddConsole();
            });            
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IWebHostEnvironment env, IServiceProvider serviceProvider)
        {
            app.UseWebSockets();
            app.UseWebSocketDaemon("/test_socket", serviceProvider.GetService<WebSocketDaemon>());

            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }

            app.UseHttpsRedirection();

            app.UseRouting();

            app.UseAuthorization();

            app.UseEndpoints(endpoints =>
                {
                    endpoints.MapControllers();
                });

            app.UseStaticFiles();
            // app.UseDefaultFiles();
            
            app.UseCors("CorsPolicy");
        }
    }
}
