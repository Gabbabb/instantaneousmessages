using System;
using System.Threading.Tasks;
using System.Net.WebSockets;
using Microsoft.AspNetCore.Http;
using System.Threading;

namespace InstantaneousMessages
{
    public class WebSocketDaemonMiddleware
    {
        readonly RequestDelegate _next;
        private readonly WebSocketDaemon _webSocketDaemon;

        public WebSocketDaemonMiddleware(RequestDelegate next, WebSocketDaemon webSocketDaemon)
        {
            this._next = next;
            this._webSocketDaemon = webSocketDaemon;
        }

        public async Task Invoke(HttpContext context)
        {
            // не делать ничего, если запрос не по сокету
            if (!context.WebSockets.IsWebSocketRequest)
                await _next.Invoke(context);
                //await context.Response.BodyWriter.WriteAsync(new ReadOnlyMemory<byte>(Encoding.UTF8.GetBytes("not socket rq")));
           
            _webSocketDaemon.Socket = await context.WebSockets.AcceptWebSocketAsync();
            var buffer = new byte[1024 * 4];
            while(_webSocketDaemon.WebSocketStateOpen())
            {
                var result = await _webSocketDaemon.Socket.ReceiveAsync(buffer: new ArraySegment<byte>(buffer), cancellationToken: CancellationToken.None);
                if (result.MessageType == WebSocketMessageType.Close)
                    await _webSocketDaemon.Socket.CloseAsync(WebSocketCloseStatus.NormalClosure, "Closed by Middleware", CancellationToken.None);
                                                
            }
            //await context.Response.BodyWriter.WriteAsync(new ReadOnlyMemory<byte>(Encoding.UTF8.GetBytes(_webSocketDaemon.WebSocketStateOpen().ToString())));
        }
    }
}
