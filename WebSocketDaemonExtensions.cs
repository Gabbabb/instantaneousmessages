using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Http;

namespace InstantaneousMessages
{
    public static class WebSocketDaemonExtensions    
    {
        public static IApplicationBuilder UseWebSocketDaemon(this IApplicationBuilder app, PathString path, WebSocketDaemon daemon)
        {        
            return app.Map(path, (_app) => _app.UseMiddleware<WebSocketDaemonMiddleware>(daemon));
        }
    }
}
